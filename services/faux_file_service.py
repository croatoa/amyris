
import os
import re
import json
from os import path
from exceptions.exceptions import FauxFileValidationException, UnsuppportedFormatException
from processing.summarizer import Summary
from processing.validator import check_validity, is_int
from processing.converter import faux_to_csv, faux_to_json, faux_to_py

FAUX_DIRECTORY = './faux_data'

class FauxFileService:

    def __init__(self):
        if not path.exists(FAUX_DIRECTORY):
            os.mkdir(FAUX_DIRECTORY)
        return


    # gets all files
    # noted that this is dangerous and a bad idea in production because the file aggregation could be big
    def get_all(self):
        files = {}
        names = os.listdir(FAUX_DIRECTORY)
        for name in names:
            with open(f'{FAUX_DIRECTORY}/{name}') as file:
                files[name] =  file.read()

        return files


    # gets a file
    def get(self, id: int):
        with open(f'{FAUX_DIRECTORY}/file_{id}.faux','r') as file:
            return file.read()


    # creates a file
    def create(self, contents: str):
        
        self.validate(contents)
        
        next_id = self._get_last_id() + 1
        with open(f'{FAUX_DIRECTORY}/file_{next_id}.faux','x') as file:
            file.write(contents)

        return next_id

    def _get_last_id(self):
        file_sequence = [(-1 if re.search(r'\d+', file) is None else int(re.search(r'\d+', file).group()))  for file in os.listdir(FAUX_DIRECTORY)]
        print(len(file_sequence))
        if len(file_sequence) > 0:
            number = sorted(file_sequence).pop() 
            return number if is_int(number) else -1  
        else:
            return -1

    #deletes a file
    def delete(self, id: int):
        if os.path.exists(f'{FAUX_DIRECTORY}/file_{id}.faux'):
            os.remove(f'{FAUX_DIRECTORY}/file_{id}.faux')
        return


    # summarizes a file
    def summarize(self, contents: str):
        
        return Summary(contents).to_JSON()

    # gets all summaries of saved files
    def get_all_summaries(self):
        files = {}
        names = os.listdir(FAUX_DIRECTORY)
        for name in names:
            with open(f'{FAUX_DIRECTORY}/{name}') as file:
                files[name] =  Summary(file.read()).to_JSON()

        return json.dumps(files)

    # gets a summary by the implicit id
    def get_summary(self,id):
        return json.dumps(Summary(self.get(id)).to_JSON())

    # determines if a faux file is valid
    def validate(self, contents: str):     
        
        validity = check_validity(contents)
        
        if(validity.is_valid):
            return
        else:
            raise FauxFileValidationException(validity.invalid_reason)

    # converts a faux file to another format
    def convert(self, format: str, contents: str):
        
        self.validate(contents)
        
        if format == 'json':
            return faux_to_json(contents)
        elif format == 'csv':
            return faux_to_csv(contents)
        elif format == 'py':
            return faux_to_py(contents)
        else:
            raise UnsuppportedFormatException(f'Output file format {format} is not supported')        