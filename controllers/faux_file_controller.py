
import sys
import re
from services.faux_file_service import FauxFileService
from exceptions.exceptions import FauxFileValidationException, UnsuppportedFormatException

class FauxFileController:

    def __init__(self,app,request,Response):
        self.app = app
        self.request = request
        self.Response = Response
        self.file_service = FauxFileService()        
        self._add_routes()
        return
    

    def _add_routes(self):

        app = self.app
        request = self.request

        # gets all faux file summaries
        @app.route('/api/summary',methods=['GET'])
        def get_summaries():
            try:
                return self.file_service.get_all_summaries(), 200        
            except BaseException as error:
                return self._format_error(error), 500


        # gets a faux file summary by the implicit id
        @app.route('/api/summary/<id>',methods=['GET'])
        def get_summary(id: int):
            try:
                return self.file_service.get_summary(id), 200        
            except BaseException as error:
                return self._format_error(error), 500

        # gets all faux files
        @app.route('/api/file',methods=['GET'])
        def get_files():
            try:
                return self.file_service.get_all(), 200        
            except BaseException as error:
                return self._format_error(error), 500


        # gets a faux file by the id in the file name (i.e. "file_{id}.faux")
        @app.route('/api/file/<id>',methods=['GET'])
        def get_file(id: int):
            try:
                return self.file_service.get(id), 200 
            except BaseException as error:
                return self._format_error(error), 500


        # saves a new faux file
        @app.route('/api/file',methods=['POST'])
        def post_file():
            try:                
                return f'{{ "resourceId": {self.file_service.create(request.json)} }}', 200 
            except FauxFileValidationException as error:
                return f'{type(error)}: {error}', 400
            except BaseException as error:
                return self._format_error(error), 500


        # deletes a faux file
        @app.route('/api/file/<id>',methods=['DELETE'])
        def delete_file(id: int):
            try:
                self.file_service.delete(id)
                return 'no content', 204         
            except BaseException as error:
                return self._format_error(error), 500


        # ascertains the validity of a faux file
        @app.route('/api/validate',methods=['PUT'])
        def validate():
            try:
                self.file_service.validate(request.json)
                return 'okay',200
            except FauxFileValidationException as error:
                return self._format_error(error), 400
            except BaseException as error:
                return self._format_error(error), 500


        # converts a faux file to a specified format
        @app.route('/api/convert/<format>',methods=['POST'])
        def convert(format:str):
            try:
                converted = self.file_service.convert(format, request.json)
                return self.Response(
                    converted,
                    mimetype=f'text/{format}',
                    headers={f'content-disposition': 'attachment;  filename=faux_converted.{format}'}
                )
                
            except UnsuppportedFormatException as error:
                return self._format_error(error), 400
            except FauxFileValidationException as error:
                return self._format_error(error), 400
            except BaseException as error:
                return self._format_error(error), 500


    def _format_error(self,error):
        error_class = (re.search('\'\w+\'', str(error), re.IGNORECASE) or [''])[0]
        return f'{error_class}: {error}'