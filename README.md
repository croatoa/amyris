# amyris - code exercise

## running
- navigate to the root directory
- run 'flask run' or 'python app.py'
- in a web browser, access the UI at http://localhost:5000
- if not using the browser, the APIs (written as web APIs) can be callable from anywhere through as a HTTP REST service 