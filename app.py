from flask import Flask
from flask import request
from flask import Response
from flask import render_template
from flask import send_file
from controllers.faux_file_controller import FauxFileController

app = Flask(__name__)

@app.route('/')
def index():    
    return render_template('index.html')

@app.route('/<path:name>.js')
def module(name):    
    return send_file(f'static/{name}.js',mimetype='application/javascript')

def register_controllers():
    FauxFileController(app, request, Response)

register_controllers()


if __name__ == '__main__':
    app.run(debug=True)
