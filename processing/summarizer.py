from processing.validator import check_validity, is_row_invalid, is_int, is_float

class Summary():
    def __init__(self, contents):        
        
        validity = check_validity(contents)

        rows = list(filter(lambda row: len(row.strip()) > 0, contents.split('\n')))

        if validity.is_valid or validity.invalid_reason != 'missing header':
            rows = rows[1:]   


        fauxness_sum = 0
        fauxness_count = 0
        real_count = 0
        fake_count = 0
        ambiguous_count = 0
        for row in rows:
            if not is_row_invalid(row):
                values = list(map(lambda v: v.strip(), row.split(',')))
                if is_float(values[2]):
                    fauxness_sum += float(values[2])
                    fauxness_count += 1
                if values[3] == 'real':
                    real_count += 1
                elif values[3] == 'fake':
                    fake_count += 1
                elif values[3] == 'ambiguous':
                    ambiguous_count += 1;

        self.average_fauxness = fauxness_sum / fauxness_count if fauxness_count > 0 else 0
        self.real_count = real_count
        self.fake_count = fake_count
        self.ambiguous_count = ambiguous_count

        self.is_valid = validity.is_valid
        self.invalid_reason = validity.invalid_reason     

        self.sample_count = len(rows)

    def to_JSON(self):
        return {
            'sampleCount': self.sample_count,
            'averageFauxness': self.average_fauxness,
            'realCount': self.real_count,
            'fakeCount': self.fake_count,
            'ambiguousCount': self.ambiguous_count,
            'isValid': self.is_valid,
            'invalidReason': self.invalid_reason
        }
    