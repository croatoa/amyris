import json


def faux_to_csv(contents: str):
    return contents.strip()


    
def faux_to_json(contents: str):
    return json.dumps(_to_list(contents)) 



def faux_to_py(contents: str):
    rows = _to_list(contents)
    output = 'def faux_data():\n'
    output += '\trows = []\n'
    for row in rows:
        output += '\trows.append({\n'
        output += f'\t\t"experiment_name": "{row["experiment_name"]}",\n'
        output += f'\t\t"sample_id": {row["sample_id"]},\n'
        output += f'\t\t"fauxness": {row["fauxness"]},\n'
        output += f'\t\t"category_guess": "{row["category_guess"]}"\n'
        output += '\t})\n'

    output += '\treturn rows\n'
    return output

def _to_list(contents: str):
    rows = list(filter(lambda row: len(row.strip()) > 0, contents.split('\n')))
    keys = list(map(lambda v: v.strip(), rows[0].split(',')))
    output = []
    for row in rows[1:]:
        values = list(map(lambda v: v.strip(), row.split(',')))
        output.append({
            keys[0]: values[0],
            keys[1]: values[1],
            keys[2]: values[2],
            keys[3]: values[3]
        })
    return output