
import re

class Validity:
  def __init__(self, is_valid, reason = ''):
    self.is_valid = is_valid
    self.invalid_reason = reason


def check_validity(file_contents: str) -> Validity:
    if not file_contents.strip():
        return Validity(False, 'missing contents')
    
    rows = list(filter(lambda row: len(row.strip()) > 0, file_contents.split('\n')))

    if _missing_header(rows[0]):
        return Validity(False, 'missing header')

    for row in rows[1:]:
        if(_row_is_truncated(row)):
            return Validity(False, 'row is truncated')
        elif(_row_has_too_many_fields(row)):
            return Validity(False, 'row has too many fields')
        elif _row_is_disordered(row):       
            return Validity(False, 'row is disordered')

    return Validity(True)


def is_row_invalid(row):
    return _row_is_truncated(row) or _row_has_too_many_fields(row) or _row_is_disordered(row)

def is_int(n):
    try:
        int(n)
        return True
    except ValueError:
        return False

def is_float(n):
    try:
        float(n)
        return True
    except ValueError:
        return False


def _missing_header(row):
    return not re.search(r'( *)experiment_name( *),( *)sample_id( *),( *)fauxness( *),( *)category_guess( *)', row, re.IGNORECASE)

def _row_is_truncated(row):
    return row.count(',')  < 3
    
def _row_has_too_many_fields(row):
    return row.count(',')  > 3

def _row_is_disordered(row):
    # assumption
    # -- we can classify a row as disordered if a field value is out of range or does not match the necessary pattern
    # -- its a general validation test, but there is no discernment between invalid data and disordered data, exactly. 
    # -- (a disordering could maybe be assumed by types (e.g. whethter the value is an int, decimal/float/string etc))

    values = list(map(lambda v: v.strip(), row.split(',')))
    
    #experiment name -- exp_{n > 0} str
    if len(values[0]) == 0:
        return True
    #sample id -- n > 0 int
    if not is_int(values[1]) or int(values[1]) < 1:
        return True
    #fauxness -- f >= 0 && f =< 1
    if not is_float(values[2]) or float(values[2]) < 0 or float(values[2]) > 1:
        return True
    #category guess -- str
    if values[3].lower() not in ['real', 'fake', 'ambiguous']:
        return True

    return False