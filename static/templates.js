const templates = {
    visit: x => `
        <visit>
            Visit the <a href='https://gitlab.com/croatoa/amyris' target='_blank'>git repository <paperclips>🖇</paperclips></a>
        </visit>`,
    requirements: x => `
        <requirements>
            <span>
                Your task is to write a Python API with the following abilities:
            </span>
            <ul>
                <li>Parse and validate a Fauxlizer data file, rejecting invalid data files.</li>
                <li>Provide a summary of the data in the file in JSON format.</li>
                <li>    
                    Return a particular row of data in several formats:
                    <ul>
                        <li>Fauxlizer's native csv format</li>
                        <li>JSON</li>
                        <li>A Python in-memory representation</li>
                    </ul>
                </li>
            </ul>
        </requirements>`,    
    uploader: x => `
        <upload>
            <browse>
                browse for file
                <input id='file-add' type='file' accept='.faux' />
            </browse>
            <generate>generate random file</generate>
            <spacer>&nbsp;</spacer>
            <enabler>enable editing</enabler>
        </upload>
        <pastein>
            <textarea id='text-add' disabled placeholder='(file preview)' spellcheck='off'></textarea>               
            <save>
                <button id='go-save' disabled>validate & save</button>
                <spacer>&nbsp;</spacer>
                <status>(awaiting input)</status>
            </save>
        </pastein>
        <svmmary></svmmary>`,
    summaries: x => x.map(s => 
        `<tr id='summary-${s.resourceId}' class='${s.isValid ? '' : 'invalid'}'>
            <td>${s.key}</td>
            <td>${s.isValid ? 'valid' : 'invalid'}</td>
            <td>${s.invalidReason}</td>
            <td>${s.sampleCount}</td>
            <td>${isNaN(s.averageFauxness) ? s.averageFauxness : s.averageFauxness.toFixed(4)}</td>
            <td>${s.realCount}</td>
            <td>${s.fakeCount}</td>
            <td>${s.ambiguousCount}</td>
            <td>
                <download data-resourceid='${s.resourceId}'>
                    <csv>csv</csv>
                    <json>json</json>
                    <py>py</py>
                </download>
            </td>
            <td>
                <delete data-resourceid='${s.resourceId}'>
                    delete
                </delete>
            </td>
        </tr>`
    ).join(''),
    summaryTable: x => `
        <table>
            <thead>
                <tr class='spacer'>                    
                </tr>
                <tr>
                    <th>
                        file name
                    </th>
                    <th>
                        validity
                    </th>
                    <th>
                        invalid reason
                    </th>
                    <th>
                        sample count
                    </th>
                    <th>
                        average fauxness
                    </th>
                    <th>
                        real samples
                    </th>
                    <th>
                        fake samples
                    </th>
                    <th>
                        ambiguous samples
                    </th>     
                    <th>
                        download as
                    </th>
                    <th>
                        &nbsp;
                    </th>                   
                </tr>
            </thead>
            <tbody>${ x ? templates.summaries([x]) : ''}</tbody>
        </table>`,
    message: x => `
        <message class='${x.status}'>
            <h4>${x.title}</h4>
            <pre>${JSON.stringify(x.message,undefined,2)}</pre>
        </message>`,
}


export { templates }