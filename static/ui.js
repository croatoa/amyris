import { network, file } from './pianowire.js';
import { templates } from './templates.js';

network.host = 'http://localhost:5000';

const INFORMATION = 'information';
const ACTION = 'action';
const SUCCESS = 'success';
const ERROR = 'error';


/*
    user event handlers
*/
const handleInput = e => {
    enableSave();
}
const handleChange = e => {
    switch(e.target.id){
        case 'text-add': enableSave(); break;
        case 'file-add': trySaveFile(); break;
    }
}
const handleClick = e => {
    switch(e.target.id || e.target.nodeName){
        case 'MESSAGE': 
        case 'MESSAGER': 'messager'.deactivate(); break;        
        case 'BROWSE': '#file-add'.click(); break;     
        case 'GENERATE':generateFauxData(); break;   
        case 'ENABLER': toggleEditMode(); break;
        case 'CSV': convert('csv',e.target.parentElement.dataset.resourceid); break;
        case 'JSON': convert('json',e.target.parentElement.dataset.resourceid); break;
        case 'PY': convert('py',e.target.parentElement.dataset.resourceid); break;
        case 'DELETE': confirmDelete(e.target.dataset.resourceid); break;
        case 'REQUIREMENTS': alert('NOT IMPLEMENTED'+6); break;
        case 'go-save': !e.target.disabled && trySaveText(); break;
    }
}

/*
    user event listeners
*/
document.listen('input',handleInput);
document.listen('change',handleChange);
document.listen('click',handleClick);


/*
    api reads
*/
const getSummary = async id => await network.get(`api/summary/${id}`);
const getSummaries = async () => {
    'spinner'.activate();
    let summaries = await network.get('api/summary');
    if(!summaries){
       message(INFORMATION,'no summaries returned','-',5000);
    }
    else if(summaries.error){
        message(ERROR,'error loading summaries');
    } else {    
        for(let key in summaries) summaries[key].resourceId = (key.match(/\d+/) || [''])[0];    
        'tbody'.html = templates.summaries(summaries);
    }    
}
const convert = async (format,id) => {
    'spinner'.activate();
    let response = await network.get(`api/file/${id}`);
    if(response.error) {
        message(ERROR,'Error getting file for conversion',response.message);
    }
    let conversion = await network.file('post', `api/convert/${format}`, response,`file_${id}_conversion.${format}`,`text/${format}`);
    if(!conversion || conversion.error){
        message(ERROR,'Error converting file',conversion ? conversion.message : 'no response returned');
    }
    'spinner'.deactivate();
}
/*
    api writes
*/
const trySaveText = async () => {
    if(!'textarea'.value) 
        return;
    message(INFORMATION,'validating and saving','request in progress',3000);
    'spinner'.activate();
    'svmmary'.html = '';    
    let response = await network.post('api/file', 'textarea'.value);
    if(response.error){        
        message(ERROR,'validation error', response.message,5000);                        
    } else {
        let summary = await network.get(`api/summary/${response.resourceId}`);
        summary.resourceId = response.resourceId;
        summary.key = `file_${response.resourceId}.faux`;        
        'svmmary'.html = templates.summaryTable(summary);   
        'existing tbody'.html += 'svmmary'.el.$('tbody').innerHTML ;    
        message(SUCCESS,'validation success', { summary } ,7000);
    }
    'status'.html = 'messager'.html;
}
const trySaveFile = async () => {    
    let contents = (await file.read('input'.el))?.trim() || '';    
    'textarea'.value = contents;
    enableSave();
    
    //await trySaveText();
}
const confirmDelete = async id => window.confirm(`Permanently delete the file file_${id}.faux`) ?  await deleteFile(id) : null;
const deleteFile = async id => {
    let response = await network.delete(`api/file/${id}`);
    if(response.error){
        message(ERROR,'There was an error deleting this file',response.message,7000);
    } else {
        message(SUCCESS,'File deleted','',5000);
        `#summary-${id}`.el.remove();
    }
} 
/*
    miscellaneous
*/
const generateFauxData = () => {
    let rfloor = max => Math.floor(max * Math.random());  
    let rows = [['experiment_name', 'sample_id', 'fauxness', 'category_guess']];
    for(let i = 0; i < rfloor(50); i++) {
        rows.push([`tma-${rfloor(1000)}.${String.fromCharCode(rfloor(70)+45)}`,rfloor(1000),rfloor(10001)/10000,['real','ambiguous','fake'][rfloor(3)]]);
    }
    'textarea'.value = rows.map(row => row.join(',')).join('\n');
    enableSave();
}
const toggleEditMode = () => { 
    if('enabler'.isActive()) { 
        'enabler'.deactivate(); 
        'textarea'.el.disabled = true;
    } else { 
        'enabler'.activate(); 
        'textarea'.el.disabled = false;
    } 
}
const enableSave = () => {
    if('textarea'.value.trim()){
        'save button'.activate();
        'save button'.el.disabled = false;
    } else {
        'save button'.deactivate();
        'save button'.el.disabled = true;
    }
}
const message = (status,title,message,disappear) => 'messager'.html_x = templates.message({status,title,message}, disappear && setTimeout(() => 'messager'.deactivate(), disappear));
window.MESSAGE = message;
/*
    initialization
*/
(async () => {
    'spinner'.activate();
    'accoutrements'.html += templates.visit() + templates.requirements();
    'add'.html = templates.uploader();
    'existing'.html = templates.summaryTable();
    await getSummaries();    
    'spinner'.deactivate();
})();

