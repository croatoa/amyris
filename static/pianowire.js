

/*
    window functions
*/
window.location.hasQuery = function (name) {
    return window.location.search.lower.indexOf(name.lower) > -1;
}
window.location.query = function (name) {
    return ((new RegExp(`(\\?|&)${name}=\\w+`, 'i').exec(window.location.search) || [''])[0]).split('=')[1];
}
/*
    dom functions
*/
window.id = sel => document.getElementById(sel);
window.$ = sel => document.querySelector(sel);
window.$$ = sel => document.querySelectorAll(sel);

Node.prototype.toggle = function () {
    if (this.classList.contains('active')) this.classList.remove('active');
    else this.classList.add('active');
    return this;
}

Node.prototype.show = function (display) {
    this.style.display = display || 'initial';
    return this;
}
String.prototype.show = function () {
    return this.el.show();
};
Node.prototype.hide = function () {
    this.style.display = 'none';
    return this;
}
String.prototype.hide = function () {
    return this.el.hide()
};
String.prototype.click = function () {
    return this.el.click();
};
Node.prototype.listen = function (event, fn, options) {
    this.addEventListener(event, fn, options || false);
    return this;
}
Node.prototype.listenOnce = function (event, fn) {
    this.addEventListener(event, fn, { once: true });
    return this;
}
Node.prototype.listeners = function(event,fns){
    this.addEventListener(event,e => fns.forEach(fn => fn(e)));
    return this;
}

Node.prototype.$ = function (sel) {
    return this.querySelector(sel);
}

Node.prototype.$$ = function (sel) {
    return this.querySelectorAll(sel);
}

Node.prototype.attr = function (name) {
    return this.getAttribute(name);
}

let formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD'
});
Number.prototype.toMoney = function () {
    return formatter.format(this);
}
String.prototype.toMoney = function () {
    return formatter.format(this.number);
}
Number.prototype.floor = function () {
    return Math.floor(this);
}
Number.prototype.toPercent = function (dec) {
    return `${(this * 100).toFixed(dec || 2)}%`;
}
String.prototype.toPercent = function (dec) {
    return `${(Number(this) * 100).toFixed(dec || 2)}%`;
}
Number.prototype.abs = function () {
    return Math.abs(this);
}
Number.prototype.random = function () {
    return Math.random() * this;
}
Node.prototype.classUp = function (_class, maxDepth = 6) {
    if (this.classList.contains(_class)) return this;
    else {
        let el = this,
            i = 0;
        while ((el = el.parentElement) && ++i < maxDepth) {
            if (el.classList.contains(_class)) {
                return el;
            }
        }
    }
    return null;
}
Node.prototype.nodeUp = function (nodename, maxDepth = 5) {
    nodename = nodename.toUpperCase();
    if (this.nodeName == nodename) return this;
    else {
        let el = this,
            i = 0;
        while ((el = el.parentElement) && ++i < maxDepth) {
            if (el.nodeName === nodename) {
                return el;
            }
        }
    }
    return null;
}
Node.prototype.dataUp = function (attribute, comparator, maxDepth = 3) {
    if (comparator == undefined || comparator == null) {
        if (this.dataset[attribute]) {
            return this.dataset[attribute];
        } else {
            let el = this,
                i = 0;
            while ((el = el.parentElement) && ++i < maxDepth) {
                if (el.dataset[attribute]) {
                    return el.dataset[attribute];
                }
            }
            return null;
        }
    } else {
        if (this.dataset[attribute]) {
            if (this.dataset[attribute] == comparator) return this;
            else return null;
        } else {
            let el = this,
                i = 0;
            while ((el = el.parentElement) && ++i < maxDepth) {
                if (el.dataset[attribute]) {
                    if (el.dataset[attribute] == comparator) return this;
                    else return null;
                }
            }
            return null;
        }
    }
}
Node.prototype.isVisible = function () {
    return getComputedStyle(this).display != 'none';
}
Node.prototype.nextClass = function (sel) {
    let sibling = this.nextElementSibling;
    while (!sibling.classList.contains(sel) && sibling.nextElementSibling) {
        sibling = sibling.nextElementSibling;
    }
    return sibling;
}
Node.prototype.previousClass = function (sel) {
    let sibling = this.previousElementSibling;
    while (!sibling.classList.contains(sel) && sibling.previousElementSibling) {
        sibling = sibling.previousElementSibling;
    }
    return sibling;
}
Node.prototype.scroll2 = function (offset = 0) {
    let goal = this.getBoundingClientRect().y - offset,
        current = window.scrollY,
        diff = goal - current,
        down = diff > 0;

    let scroll = () => {
        if (down) {
            window.scrollTo(0, current += 5);
            if (current < goal) return window.requestAnimationFrame(scroll);
        }
        else {
            window.scrollTo(0, current -= 5);
            if (current > goal) return window.requestAnimationFrame(scroll);
        }
    }
    window.requestAnimationFrame(scroll);
    return this;
}
let getElement = sel => {    
    let el = document.getElementById(sel);
    if (!el) el = document.querySelector(sel);
    if (!el) el = document.getElementsByClassName(sel)[0];
    return el;
}
Node.prototype.__defineGetter__('x', function () {
    return this.style.left;
});
String.prototype.__defineGetter__('y', function () {
    return this.style.top;
});
String.prototype.__defineSetter__('x', function (val) {
    return this.style.left = `${val}px`;
});
String.prototype.__defineSetter__('y', function (val) {
    return this.style.top = `${val}px`;
});
String.prototype.__defineGetter__('number', function () {
    return Number(this) || 0;
});
String.prototype.__defineGetter__('el', function () {
    return getElement(this);
});
String.prototype.__defineGetter__('value', function () {
    return this.el.value;
});
String.prototype.__defineSetter__('value', function (val) {
    return this.el.value = val;
});
String.prototype.__defineGetter__('html', function () {
    return this.el.innerHTML;
});
String.prototype.__defineGetter__('text', function () {
    return this.el.innerText;
});
String.prototype.__defineSetter__('html', function (val) {
    return this.el.innerHTML = val?.shrink();
});
String.prototype.__defineSetter__('html_x', function (val) {
    this.el.innerHTML = val?.shrink();
    this.el.activate();
    return this.el;
});
String.prototype.__defineSetter__('text', function (val) {
    return this.el.innerText = val;
});
String.prototype.__defineGetter__('visible', function () {
    return this.el.isVisible();
});
String.prototype.__defineGetter__('lower', function () {
    return this.toLowerCase();
});
String.prototype.__defineGetter__('upper', function () {
    return this.toUpperCase();
});
String.prototype.listen = function (ev, fn) {
    this.el.addEventListener(ev, fn);
    return this
};
Node.prototype.activate = function () {
    this.classList.add('active');
    return this;
}
Node.prototype.deactivate = function () {
    this.classList.remove('active');
    return this;
}
Node.prototype.deaactivate = function (t) {
    setTimeout(() => this.classList.remove('active'), 1000);
    return this;
}
Node.prototype.isActive = function () {
    return this.classList.contains('active');
}
String.prototype.isActive = function () {
    return this.el?.isActive();
};
String.prototype.activate = function () {
    return this.el?.activate();
};
String.prototype.deactivate = function () {
    return this.el?.deactivate();
};
String.prototype.attr = function (key, val) {
    if(val === undefined) this.el.getAttribute(key); else this.el.setAttribute(key, val);
    return this.el;
};
String.prototype.style = function (key, val) {
    return val === undefined ? getComputedStyle(this.el)[key] : this.el.style[key] = val;
};
String.prototype.focus = function () {
    this.el.focus();
    return this;
};
String.prototype.is = function (comp) {
    return this.toString() === comp;
};
String.prototype.change = function (val) {
    if (val) {
        this.el.value = val;
    }
    this.el.dispatchEvent(new Event('change', { bubbles: true, cancelable: false }));
}
String.prototype.loop = function (fn) {
    return $$(this).forEach(fn);
}
String.prototype.hash = function () {
    // credit where credit is due
    // https://stackoverflow.com/questions/7616461/generate-a-hash-from-string-in-javascript
    var hash = 0, i, chr;
    for (i = 0; i < this.length; i++) {
        chr = this.charCodeAt(i);
        hash = ((hash << 5) - hash) + chr;
        hash |= 0; // Convert to 32bit integer
    }
    return hash;
}
String.prototype.contains = function (substring) {
    return this.indexOf(substring) > -1;
}
String.prototype.shrink = function(){
    return this.replace(/ +/g,' ');
}

String.prototype.in = function(){
    return Array.from(arguments).map(x => x.toLowerCase()).includes(this.toLowerCase());
}

window.LOG = (val1, val2) => (val2 ? console.log(val1, val2) : console.log(val1)) ? val1 : val1;

window.localStorage.__proto__.extendItem = function (key, val) {
    let existing = localStorage.getItem(key);
    if (existing) localStorage.setItem(key, `${existing},${val.toString()}`);
    else localStorage.setItem(key, val.toString());
}
window.localStorage.__proto__.spliceItem = function (key, val) {
    let existing = localStorage.getItem(key) || '';
    existing = existing.replace(val.toString(), '').replace(/^,|,,|,$/, '');
    localStorage.setItem(key, existing);
}
Object.prototype.toString = function () {
    try {
        return JSON.stringify(this);
    } catch (err) {
        return this.toString();
    }
}
Array.prototype.toString = function () {
    if (typeof this[0] === 'object') {
        try {
            return JSON.stringify(this);
        } catch (err) {
            return this.toString();
        }
    } else {
        return `[${this.join(',')}]`;
    }
}
Array.prototype.average = function () {
    let t = 0;
    for (var n of this) {
        t += n;
    }
    return (t / this.length);
}
Array.prototype.distinct = function () {
    return [...new Set(this)];
}

Array.prototype.activate = function () {
    this.forEach(n => n.activate());
    return this;
}
Array.prototype.deactivate = function () {
    this.forEach(n => n.deactivate());
    return this;
}

Object.prototype.map = function(fn){
    return Object.keys(this).map(key => {
        this[key].key = key;
        return fn(this[key]);
    });
}

/*
    /end dom functions
*/

/*
    file functions
*/

let file = {}

file.read = el => {
    return new Promise((res,rej) => {
        try {
            if(!el.files[0]) throw 'no file to read';
            
            let file = el.files[0],
                reader = new FileReader();            
            reader.onload = e => res(e.target.result);
            reader.readAsText(file);
        } catch(err){
            rej(err);
        }

    });
}


/*
    network functions
*/

const network = {};

const format = data => {
    try {        
        return JSON.parse(data.replaceAll('NaN','"NaN"'));
    } catch (err) {
        return data;
    }
}

const NO_OPERATION = () => {};
const FILE_SYSTEM = window.requestFileSystem || window.webkitRequestFileSystem;

const handle = req => {
    $('spinner')?.deactivate();
    if(req.status > 199 && req.status < 300){
        return format(req.response)
    } else {
        return { 
            error: true, 
            status: req.status, 
            message: `${req.statusText} - ${format(req.response)}` 
        }
    }
}

network.get = route => {
    $('spinner')?.activate();
    return new Promise((res, rej) => {
        let req = new XMLHttpRequest();
        req.open('get', `${network.host}/${route}`);
        req.onload = () => res(handle(req));
        req.onerror = () => rej(handle(req));
        req.send();
    });
}
network.external = uri => {
    return new Promise((res, rej) => {
        let req = new XMLHttpRequest();
        req.open('get', uri);
        req.onload = () => res(handle(req));
        req.onerror = () => rej(handle(req));
        req.send();
    });
}
network.file = (method,route,data,fileName,mime) => {
    return new Promise((res, rej) => {
        let req = new XMLHttpRequest();
        req.open(method, `${network.host}/${route}`);
        req.setRequestHeader('content-type', 'application/json');
        req.onload = e => {
            try{
                if(req.status > 199 && req.status < 300){                    
                    let anchor = document.createElement('a');                                                
                    anchor.setAttribute('target','_blank');
                    anchor.setAttribute('href',`data:text/${mime};charset=utf-8,${encodeURIComponent(req.response)}`);
                    anchor.setAttribute('download', fileName);
                    document.body.append(anchor);
                    anchor.click();
                    anchor.remove()
                    res(req.response);
                }                
                else {
                    res({ 
                        error: true, 
                        status: req.status, 
                        message: `${req.statusText} - ${format(req.response)}` 
                    })
                }                
            } catch(err) {
                rej(err);
            }
        }            
        req.onerror = () => rej(handle(req));
        req.send(data ? JSON.stringify(data) : undefined);
    });
}
network.post = (route, data) => {
    return new Promise((res, rej) => {
        let req = new XMLHttpRequest();
        req.open('post', `${network.host}/${route}`);
        req.setRequestHeader('content-type', 'application/json');
        req.onload = () => res(handle(req));
        req.onerror = () => rej(handle(req));
        req.send(JSON.stringify(data));
    });
}
network.put = (route, data) => {
    return new Promise((res, rej) => {
        let req = new XMLHttpRequest();
        req.open('put', `${network.host}/${route}`);
        req.setRequestHeader('content-type', 'application/json');
        req.onload = () => res(handle(req));
        req.onerror = () => rej(handle(req));
        req.send(JSON.stringify(data));
    });
}
network.delete = (route, data) => {
    return new Promise((res, rej) => {
        let req = new XMLHttpRequest();
        req.open('delete', `${network.host}/${route}`);
        if (data) {
            req.setRequestHeader('content-type', 'application/json');
        }
        req.onload = () => res(handle(req));
        req.onerror = () => rej(handle(req));
        req.send(data ? JSON.stringify(data) : undefined);
    });
}
/*
    /end data functions
*/


Object.prototype.toString = function () {
    return JSON.stringify(this);
}
String.prototype.toType = function () {
    try {
        return JSON.parse(this);
    } catch (err) {
        return this;
    }
}
let storage = {};
storage.get = key => (window.localStorage.getItem(key) || '').toType();
storage.set = (key, val) => window.localStorage.setItem(key, val.toString());
storage.add = (key, val) => window.localStorage.hasOwnProperty(key) ? storage.set(key,storage.get(key).concat([val])) : storage.set(key,[val]);

export { network, file, storage };

