
class FauxFileValidationException(Exception):
    pass

class UnsuppportedFormatException(Exception):
    pass